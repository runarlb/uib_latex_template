Beamer Template for UiB

Created by Runar Berge (runar.berge@uib.no)
May be freely distributed at UiB.

See uibTemplate.tex for example of how to use the template.

To be able to load the template from anywhere on your computer
you must move the template files to their respective placed
in the beamer folder, e.g., something like:

beamercolorthemeuib.sty -> /tex/latex/beamer/themes/color
bemaerinnerthemeuib.sty -> /tex/latex/beamer/themes/inner
beamerouterthemeuib.sty -> /tex/latex/beamer/themes/outer
beamerthemeUib.sty      -> /tex/latex/beamer/themes/theme

Or you can just move these files to your presentation folder.
